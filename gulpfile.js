var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	notify = require('gulp-notify'),
	uglify = require('gulp-uglify'),
	wiredep = require('gulp-wiredep'),
	useref = require('gulp-useref'),
	csso = require('gulp-csso'),
	del = require('del'),
	clean = require('gulp-clean'),
	browserSync = require('browser-sync').create();
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        styles: 'build/css/',
        assets: 'build/assets/',
        fonts: 'build/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/**/*.js',//В стилях и скриптах нам понадобятся только main файлы
        styles: 'src/styles/main.scss',
        assets: 'src/assets/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        styles: 'src/styles/**/*.{css,scss}',
        assets: 'src/assets/**/*.*',
        fonts: 'src/fonts/**/*.*',
		bower: './bower.json'
    },
	clean: './build'
};

gulp.task('default', ['clean'], function(){
	gulp.run('dev')
});

gulp.task('production', ['clean'], function(){
	gulp.run('BuildProd')
});

gulp.task('BuildProd', ['buildHTML', 'buildCSS', 'buildAssets', 'buildFonts']);

gulp.task('dev', ['build', 'watch', 'browser-sync']);

gulp.task('build', ['buildCSS','HTML', 'buildAssets', 'buildJS', 'buildFonts']);

gulp.task('watch', function() {
	gulp.watch(path.watch.styles, ['buildCSS']);
    gulp.watch(path.watch.js, ['buildJS']);
    gulp.watch(path.watch.html, ['HTML']);
    gulp.watch(path.watch.assets, ['buildAssets']);
    gulp.watch(path.watch.fonts, ['buildFonts']);
});

gulp.task('HTML', function(){
	gulp.src(path.src.html)
	.pipe(wiredep({
		directory: 'bower_components/'
	}))
	.pipe(gulp.dest(path.build.html))
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('buildHTML', function(){
	gulp.src(path.src.html)
	.pipe(wiredep({
		directory: 'bower_components/'
	}))
	.pipe(useref())
	.pipe(gulp.dest(path.build.html))
		.on('end', function () {
			gulp.run('minJS');
        })
});

gulp.task('minJS', function(){
    gulp.src('build/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
});


gulp.task('buildCSS', function(){
	gulp.src(path.src.styles)
	.pipe(plumber({
			errorHandler: notify.onError(function(err) {
				return {
					title: 'Styles',
					message: err.message
				}
			})
		}))
	.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
		.pipe(csso())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(path.build.styles))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('buildAssets', function(){
	gulp.src(path.src.assets)
	.pipe(gulp.dest(path.build.assets))

});

gulp.task('buildFonts', function(){
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('clean', function(){
    return gulp.src(path.clean)
        .pipe(clean())
});

gulp.task('buildJS', function() {
    gulp.src(path.src.js)
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'JS',
                    message: err.message
                }
            })
        }))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('browser-sync', function() {
	browserSync.init({
		server:{
			baseDir: './build/'
		}
	});
});
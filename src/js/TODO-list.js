$(function () {
    var input = $('.aside-content__input');
    $('.aside-content__input--accept').click(function () {;
        if(($(input).val()) == ""){
            return;
        }else{
            $('.aside-content-list')
                .append(
                '<li class="aside-content-list__item">' +
                '<p class="aside-content-list__item--text aside-content-list__item--task">'+ $('.aside-content__input').val()+'</p>' +
                '<a class="aside-content-list__item--text aside-content-list__item--redact"><i class="fa fa-pencil" aria-hidden="false"></i></a>' +
                '<a class="aside-content-list__item--text aside-content-list__item--delete"><i class="fa fa-times" aria-hidden="true"></i></a>' +
                '</li>'
            );
            $(input).val('');

        }
    });

    $('.aside-content__input--cancel').click(function () {
        $('.aside-content__input').val(' ');
    });

    $('.aside-content-list').on('click', '.aside-content-list__item--delete', function () {
        $(this).parent().remove();
    });
    /*

    $('.aside-content-list').on('click', '.aside-content-list__item--redact', function () {
        var test = $(this).siblings('.aside-content-list__item--task');
        $('.aside-content__input').val($(this).siblings('.aside-content-list__item--task').text());

        $('.aside-content__input--accept').click(function () {

        })

    });
       */



});
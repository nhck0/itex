$(function () {
    $('.accordion-form__info--desc').hide();

    $('.accordion-form__characteristics').click(function () {
        $('.accordion-form-field').fadeToggle(300);
        $('.accordion-form__characteristics').toggleClass('accordion-form__characteristics--active');
    });

    $('.accordion-form__info').click(function () {
        $('.accordion-form__info--desc').fadeToggle(300);
        $('.accordion-form__info').toggleClass('accordion-form__info--active');
    });

    $('.accordion-form-field__input:checkbox').on('change', function () {
        var sum = 0;
        $('.accordion-form-field__input:checked').each(function (index, elem) {
            var add = parseInt($(elem).val(), 10);
            if(!isNaN(add))
                sum += add;
        });
        $('.accordion-form__order--result').text(sum + 'Р.')
    })
});
ymaps.ready(init);
var myMap;

function init(){
    myMap = new ymaps.Map("map", {
        center: [56.86420955455974,60.61985951653279],
        zoom: 16,
        controls: []
    });

    myPlacemark = new ymaps.Placemark([56.86420955455974,60.61985951653279], {
        hintContent: 'г.Екатеринбург ул.Маяковского 25A', balloonContent: '(ノಠ益ಠ)ノ彡  \n' });

    myMap.behaviors.disable(['scrollZoom']);

    myMap.geoObjects.add(myPlacemark);
}